NED Text Editor for NES
=======================

NED is a simple text editor for NES, 
featuring 128 symbols (some invisible)
for displaying with a NES emulator

Requirements
============

* CC65
* A NES emulator (FCEUX, Nestopia, etc.)

How to compile
==============

Open the directory `src` in this package 
and type `sh compile.sh`.

How to use
==========

At the root of this package, open `ned.nes`.
With an emulator, configure the joysticks and 
press the following buttons:

* ←↓↑→: Change the character
* A: Write the character
* B: Delete the last character

Licence and Version
===================

See LICENCE.txt and VERSION.txt