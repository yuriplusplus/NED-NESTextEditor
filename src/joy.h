/*
 * Version: 0.1
 * Copyright (c) 2015 Yuri da Silva
 * NED is a free and open-source software:
 * you can redistribute it or modify it
 * under the terms of the MIT licence.
 * 
 * Note: this program is distributed
 * without warranty. See the the file
 * LICENCE.txt for more informations.
 */

#include<nes.h>

#define BTN_A  0x80
#define BTN_B  0x40
#define BTN_SE 0x20
#define BTN_ST 0x10
#define BTN_UP 0x08
#define BTN_DO 0x04
#define BTN_LE 0x02
#define BTN_RI 0x01

#define presskey(k) (key & (k))
#define read_joystick_1() read_joystick(0x4016)
#define read_joystick_2() read_joystick(0x4017)

#define ADDR(add) (*(u8*)(add))
typedef unsigned char u8;


u8 read_joystick(unsigned int _joystick) {
	u8 n = 8, joy_state = 0;
	ADDR(_joystick) = 0x01;
	ADDR(_joystick) = 0x00;
	while(n) {
		joy_state = (joy_state << 1) | (*(u8*)(_joystick)) & 1;
		--n;
	}
	return joy_state;
}

void delay(unsigned int i) {
	while(i--) {
		waitvblank();
	}
}
