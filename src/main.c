/*
 * Version: 0.1
 * Copyright (c) 2015 Yuri da Silva
 * NED is a free and open-source software:
 * you can redistribute it or modify it
 * under the terms of the MIT licence.
 * 
 * Note: this program is distributed
 * without warranty. See the the file
 * LICENCE.txt for more informations.
 */

#include"joy.h"
#include<conio.h>
#include<stdlib.h>

u8 txtpos = 0;  /*how many has been written?*/
int select = 0; /*which char is chosen?*/

/**
 * This function draws the keyboard
 * with 127 keys
 */
void drawkeys() {
	u8 letter;
	u8 i;
	clrscr();
	gotoxy(0, 0);
	for(letter = 0; letter < 32; ++letter) {
		if(letter == 10) {
			/*prevent printing the newline*/
			cprintf(" ");
			continue;
		}
		cprintf("%c", letter);
	}
	for(i = 2; i < 7; i += 2) {
		gotoxy(0, i);
		while(letter < 32 * (i/2 + 1)) {
			cprintf("%c", letter);
			++letter;
		}
	}
}

/*
 * This function clears the cursor
 * Useful for changing letters
 **/
void clearpart() {
	u8 i, j;
	for(i = 1; i < 8; i += 2) {
		for(j = 0; j < 32; ++j) {
			gotoxy(j, i);
			cprintf(" ");
		}
	}
}

/**
 * Draws the cursor
 */
void drawselect() {
	int x = select % 32;
	int y = ((select + 32) / 32) * 2 - 1;
	gotoxy(x, y);
	cprintf("%c", 'A');
}

int main() {
	u8 key;
	u8 oldkey;
	u8 isarrow = 0;
	delay(10);
	drawkeys();
	clearpart();
	drawselect();
	while(1) {
		oldkey = key;
		while(oldkey == key && !isarrow) {
			key = read_joystick_1();
			if(key == BTN_LE || key == BTN_RI) {
				isarrow = 1;
			}
		}
		if(presskey(BTN_UP)) {
			if(select >= 32) {
				select -= 32;
			}
		} else if(presskey(BTN_DO)) {
			if(select < 96) {
				select += 32;
			}
		} else if(presskey(BTN_RI)) {
			if(select < 127) {
				select += 1;
			}
		} else if(presskey(BTN_LE)) {
			if(select > 0) {
				select -= 1;
			}
		} else if(presskey(BTN_A)) {
			gotoxy(txtpos % 32 ,8 + (txtpos + 32) / 32);
			cprintf("%c", select);
			++txtpos;
		} else if(presskey(BTN_B)) {
			gotoxy(txtpos % 32, 8 + (txtpos + 32) / 32);
			cprintf(" ");
			if(txtpos > 0) {
				--txtpos;
			}
		}
		clearpart();
		drawselect();
		isarrow = 0;
		delay(1);
	}
	while(1) {}
	return 0;
}
